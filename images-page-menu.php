<?php get_header(); ?>  

<!-- scrollspy -->
<style>
  @media (min-width: 979px) {
    #mynav.affix-top {
      position: static;
      margin-top:30px;
      width:150px;
    }

    #mynav.affix {
      position: fixed;
      top:100px;
      width:150px;
    }
  }
  #mynav li.active a {
    background-color: #81B33A;
    color: white;
  }


	.dl-horizontal dt {
	  width: 195px;
	}

	.dl-horizontal dd {
	margin-left: 214px;
	}

#mynav li {
margin-bottom: 10px;
}

.jumbotron > h1.hero-header-text {
	text-transform: uppercase;
    font-size: 50px;
    font-weight: bold;
    margin-top: 22px;
}
}

</style>

<script>
  jQuery('body').scrollspy({ target: '#sidebar', offset: 70 });
  var clicked = false;
  jQuery('#mynav li a').click(
    function(){
      jQuery('#mycontent > div > h2').css('padding-top',0);
      jQuery(jQuery( this ).attr('href') + ' > h2').css('padding-top','800px');
      clicked = true;
    });  

  jQuery('body').on('activate.bs.scrollspy', function () {
    console.log('scrolling...');
    if(!clicked)jQuery('#mycontent > div > h2').css('padding-top',0);
    clicked = false;
  })  
</script>


<section id="menu-hero" class="primary-color-background hidden-xs">
  <div id="menu-hero-background" class="jumbotron primary-color-background">
    <div class="container">
      <!-- hero text -->
      <div style="z-index:5;" class="row">
        <div class="col-sm-12 text-center">
          <svg height="500" width="500" style="z-index:-1; margin-bottom:-300px;">
            <defs>
              <pattern id="image" patternUnits="userSpaceOnUse" height="100" width="100">
                <image x="0" y="0" height="500" width="500" xlink:href="http://i.imgur.com/7Nlcay7.jpg"></image>
              </pattern>
            </defs>
            <circle cx="250" cy="250" r="200" stroke="white" stroke-width="3" fill="#81B33A" />
          </svg>
          <h1 class="bold-white hero-header-text animated bounceIn">Menu</h1>
          <!-- <p class="white animated bounceIn"></p> -->
          <!-- <h2 class="bold-white"><i class="fa fa-phone white animated bounceIn"></i> 01 201 0868</h2> -->
        </div>
      </div>
    </div>
  </div>
</section>    


<section id="menu">
  <div class="container" id="mycontentcon">
    <div class="row">
      <div style="z-index:3;" class="col-md-2 hidden-sm hidden-xs">
        <div class="list-group navbar" id="sidebar">
          <ul class="nav" id="mynav" data-spy="affix" data-offset-top="450">
            <li>
              <a href="#starters" class="list-group-item">
                Starters
              </a>
            </li>
            <li> 
              <a href="#chicken" class="list-group-item" contenteditable="false">
                Chicken
              </a>
            </li>
            <li>
             <a href="#meat" class="list-group-item" contenteditable="false">
               Meat
             </a>
           </li>
           <li> 
            <a href="#prawn" class="list-group-item" contenteditable="false">
              Prawn
            </a>
          </li>
          <li> 
            <a href="#biryani" class="list-group-item" contenteditable="false">
              Biryani
            </a>
          </li>
          <li> 
            <a href="#vegetarian" class="list-group-item" contenteditable="false">
              Vegetarian
            </a>
          </li>
          <li> 
            <a href="#breads" class="list-group-item" contenteditable="false">
              Breads
            </a>
          </li>
          <li> 
            <a href="#drinks" class="list-group-item" contenteditable="false">
              Drinks
            </a>
          </li>
          <li> 
            <a href="#frozen" class="list-group-item" contenteditable="false">
              Frozen Food
            </a>
          </li>
        </ul>
      </div>
    </div>
    


    <div class="col-md-10 col-sm-12 col-xs-12" id="mycontent">
      
      <!-- starters-header -->
      <div style="padding-top:60px;" id="starters-header">
        <img src="http://placehold.it/945x250" alt="" class="img-responsive">
      </div>


      <div id="starters">
        <h2>Starters</h2>
          <div class="table-responsive"> 
            <table class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>Dish</th>
                  <th>Description</th>
                  <th>Spice Level</th>
                  <th>Allergy Code</th>
                  <th>Price</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td style="min-width:165px;"><h4>Indian Spice Wings</h4></td>
                  <td>Chicken wings marinated in ginger, paprika & garlic, deep fried and coated in a honey, soy sauce, sesame seed and chili sauce. Served with a mint dip.</td>
                  <td>Medium</td>
                  <th>DF, N</th>
                  <th>€5.95</th>
                </tr>
                <tr>
                  <td style="min-width:165px;"><h4>Real Samosas</h4></td>
                  <td>Spiced vegetables & peanuts in homemade pastry, deep fried. Served with tamarind dip.</td>
                  <td>Medium</td>
                  <th>V, DF, N</th>
                  <th style="min-width:75px;">
                    1 - €2.75<br/>
                    2 - €4.50
                  </th>
                </tr>
                <tr>
                  <td style="min-width:165px;"><h4>Onion Bhaji</h4></td>
                  <td>Our home made onion fritters with fresh mint & coriander dip.</td>
                  <td>Mild</td>
                  <th>GF, V, DF, N</th>
                  <th>€3.95</th>
                </tr>
                <tr>
                  <td style="min-width:165px;"><h4>Pakoras</h4></td>
                  <td>Indian street food. Sliced vegetables fried in gram flour batter.</td>
                  <td>Mild</td>
                  <th>V, DF, N</th>
                  <th>€3.95</th>
                </tr>
                <tr>
                  <td style="min-width:165px;"><h4>Pepper Exchange Prawns</h4></td>
                  <td>Prawns stir fried with tomato, coconut, black pepper, lime juice and coriander</td>
                  <td>Mild</td>
                  <th>GF</th>
                  <th>€6.95</th>
                </tr>
                <tr>
                  <td style="min-width:165px;"><h4>Poppadoms</h4></td>
                  <td>Sundried poppadoms cooked on premises & served with mint dip</td>
                  <td>-</td>
                  <th>GF, V, DF</th>
                  <th>€1.95</th>
                </tr>
                <tr>
                  <td style="min-width:165px;"><h4>Afghan Chicken</h4></td>
                  <td>Chicken on the bone marianted in cream cheese, yogurt, cashew paste, fenugreek leaves, fresh ginger & garlic, baked in our clay oven.</td>
                  <td>Mild</td>
                  <th>N</th>
                  <th>€5.50</th>
                </tr>
                <tr>
                  <td style="min-width:165px;"><h4>Sambusas</h4></td>
                  <td>Persian street food pastry. Miced local irish lamb, scallions, parsley, turmeric, black pepper in pastry and deep fried. Served with mint and cumin dip.</td>
                  <td>Mild</td>
                  <th>N</th>
                  <th>1 - €2.95<br>
                  2 - €5.80</th>
                </tr>
                <tr>
                  <td style="min-width:165px;"><h4>Starter Combo A</h4></td>
                  <td>2 samosas, 2 bhajias & dips.</td>
                  <td></td>
                  <th>N, V, DF</th>
                  <th>€7.60</th>
                </tr>
                <tr>
                  <td style="min-width:165px;"><h4>Starter Combo B</h4></td>
                  <td>2 samosas, 2 bhajias, 2 sambusas & dips.</td>
                  <td></td>
                  <th>N, DF</th>
                  <th>€12.80</th>
                </tr>
              </tbody>
            </table>
          </div>
        <hr class="col-md-12">
      </div>


      <!-- chicken-header -->
      <div id="chicken-header">
        <img src="http://placehold.it/945x250" alt="" class="img-responsive">
      </div>


      <div class="menu-sections" id="chicken">
        <h2>Chicken (Murgh)</h2>
          <div class="table-responsive"> 
            <table class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>Dish</th>
                  <th>Description</th>
                  <th>Spice Level</th>
                  <th>Allergy Code</th>
                  <th>Price</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td style="min-width:165px;"><h4>Murgh Makhani (Butter Chicken)</h4></td>
                  <td>Marinated chicken breast in yoghurt, paprika, cashew nuts, cardamom, cinnamon, ginger & spices, with a little butter.</td>
                  <td>Mild</td>
                  <th>GF, N</th>
                  <th>€10.95</th>
                </tr>
                <tr>
                  <td style="min-width:165px;"><h4>Sri Lankin Chicken</h4></td>
                  <td>Chicken breast pieces, coconut milk, curry leaves, crushed black pepper & fresh coriander.</td>
                  <td>Medium</td>
                  <th>GF, DF</th>
                  <th>€10.95</th>
                </tr>
                <tr>
                  <td style="min-width:165px;"><h4>Mumbai Chicken Korma</h4></td>
                  <td>Rich and fragrant chicken dish with coconut, ground almonds, sultanas & cream.</td>
                  <td>Mild</td>
                  <th>GF, N</th>
                  <th>€10.95</th>
                </tr>
                <tr>
                  <td style="min-width:165px;"><h4>Murgh Kadhai</h4></td>
                  <td>Dark and spicy dish of chicken breast & sweet peppers.</td>
                  <td>Medium</td>
                  <th>GF, LC</th>
                  <th>€10.95</th>
                </tr>
                <tr>
                  <td style="min-width:165px;"><h4>Chicken Vindaloo</h4></td>
                  <td>Our hottest chicken dish with wine vinegar, potato, coconut, garlic, ginger & chillies.</td>
                  <td>Hot</td>
                  <th>GF</th>
                  <th>€10.95</th>
                </tr>
                <tr>
                  <td style="min-width:165px;"><h4>Chicken Madras</h4></td>
                  <td>Spicy dish with coconut, black pepper, curry leaves, mustard seeds and chillies.</td>
                  <td>Medium</td>
                  <th>GF</th>
                  <th>€10.95</th>
                </tr>
                <tr>
                  <td style="min-width:165px;"><h4>Chicken Tikka Masala</h4></td>
                  <td>Chicken in creamy tomato, fenugreek, cashew nut and cardamom sauce.</td>
                  <td>Mild</td>
                  <th>N, GF</th>
                  <th>€10.95</th>
                </tr>
                <tr>
                  <td style="min-width:165px;"><h4>Curry Chicken by Mrs Singh</h4></td>
                  <td>Our chef's mum's recipe. This is how it should taste.</td>
                  <td>Mild</td>
                  <th>GF, LC</th>
                  <th>€10.95</th>
                </tr>
                <tr>
                  <td style="min-width:165px;"><h4>Chicken Jalfrezi</h4></td>
                  <td>Chicken breast marinated in tangy & sweet tomato sauce with mixed peppers & red onion.</td>
                  <td>Medium</td>
                  <th>GF</th>
                  <th>€10.95</th>
                </tr>
              </tbody>
            </table>
          </div>
        <hr class="col-md-12">
      </div>

      <!-- meat-header -->
      <div id="meat-header">
        <img src="http://placehold.it/945x250" alt="" class="img-responsive">
      </div>


      <div class="menu-sections" id="meat">
        <h2>Meat (Gosht)</h2>
        <div class="table-responsive"> 
            <table class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>Dish</th>
                  <th>Description</th>
                  <th>Spice Level</th>
                  <th>Allergy Code</th>
                  <th>Price</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td style="min-width:165px;"><h4>Lamb Kofta Chilli Masala</h4></td>
                  <td>Minced lamb meatbals in a yoghurt based sauce with fresh coriander, mint & green chillies.</td>
                  <td>Hot</td>
                  <th>GF</th>
                  <th>€10.95</th>
                </tr>
                <tr>
                  <td style="min-width:165px;"><h4>Gosht Saagwalla</h4></td>
                  <td>Diced local lamb with spinach, sundried fenugreek leaves, garlic & ginger</td>
                  <td>Medium</td>
                  <th>GF</th>
                  <th>€11.95</th>
                </tr>
                <tr>
                  <td style="min-width:165px;"><h4>Beef Madras</h4></td>
                  <td>Spicy dish of tender Irish beef with coconut, black pepper, curry leaves, mustard seeds & chillies.</td>
                  <td>Medium</td>
                  <th>GF</th>
                  <th>€10.95</th>
                </tr>
                <tr>
                  <td style="min-width:165px;"><h4>Lamb Madras</h4></td>
                  <td>Spicy dish with coconut, black pepper, curry leaves, mustard seeds & chillies.</td>
                  <td>Medium</td>
                  <th>GF</th>
                  <th>€11.95</th>
                </tr>
                <tr>
                  <td style="min-width:165px;"><h4>Lamb Korma</h4></td>
                  <td>Rich and fragrant lamb dish with coconut, ground almonds, sultanas & cream. </td>
                  <td>Mild</td>
                  <th>GF, N</th>
                  <th>€11.95</th>
                </tr>
                <tr>
                  <td style="min-width:165px;"><h4>Railway Lamb</h4></td>
                  <td>Our Rogan Josh signature dish - Diced local lamb slowly simmered with tomatoes, red onions, garam masala & spices, finished with local coriander.</td>
                  <td>Medium</td>
                  <th>GF, LC</th>
                  <th>€11.95</th>
                </tr>
                <tr>
                  <td style="min-width:165px;"><h4>Beef Vindaloo</h4></td>
                  <td>Fiery and tangy Goan dish. Tender strips of beef with potato, wine vinegar, coconut, garlic, ginger & chillies.</td>
                  <td>Hot</td>
                  <th>GF</th>
                  <th>€11.95</th>
                </tr>
                <tr>
                  <td style="min-width:165px;"><h4>Balti Beef</h4></td>
                  <td>Tender Irish beef in a red pepper, tomato, red onion based sauce with fenugreek, fresh coriander and garlic.</td>
                  <td>Medium</td>
                  <th>GF</th>
                  <th>€11.95</th>
                </tr>
              </tbody>
            </table>
          </div>
        <hr class="col-md-12">
      </div>

      <!-- prawn-header -->
      <div id="prawn-header">
        <img src="http://placehold.it/945x250" alt="" class="img-responsive">
      </div>

    
      <div class="menu-sections" id="prawn">
        <h2>Prawn (Jinga)</h2>
        <p>Served with steamed basmati rice or plain naan.</p>
        <div class="table-responsive"> 
            <table class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>Dish</th>
                  <th>Description</th>
                  <th>Spice Level</th>
                  <th>Allergy Code</th>
                  <th>Price</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td style="min-width:165px;"><h4>Prawn Jalfrezi</h4></td>
                  <td>Tiger prawns in a tangy sweet tomato sauce with mixed peppers & sweet red onions.</td>
                  <td>Medium</td>
                  <th>GF</th>
                  <th>€12.95</th>
                </tr>
                <tr>
                  <td style="min-width:165px;"><h4>Goan Prawn Curry</h4></td>
                  <td>Popular dish with tiger prawns, coconut, tamarind, chillies & curry leaves.</td>
                  <td>Medium</td>
                  <th>GF, LC</th>
                  <th>€12.95</th>
                </tr>
                <tr>
                  <td style="min-width:165px;"><h4>Jinga Makhani</h4></td>
                  <td>Tiger prawns in a rich, creamy tomato sauce with smoked paprika, crushed fenugreek leaves and toasted cashew nuts.</td>
                  <td>Medium</td>
                  <th>GF, N</th>
                  <th>€12.95</th>
                </tr>
              </tbody>
            </table>
          </div>
        <hr class="col-md-12">
      </div>

      <!-- biryani-header -->
      <div id="biryani-header">
        <img src="http://placehold.it/945x250" alt="" class="img-responsive">
      </div>

      
      <div class="menu-sections" id="biryani">
        <h2>Biryani</h2>
        <p>The complete dish. Made with basmati rice, peppers, mushrooms, tomatoes, biryani spices and cashew nuts. You select the main ingredient from chicken, lamb, prawn or vegetable. Served with a mint raita dip and poppadom.</p>
        <div class="table-responsive"> 
            <table class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>Dish</th>
                  <th>Spice Level</th>
                  <th>Allergy Code</th>
                  <th>Price</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td style="min-width:165px;"><h4>Chicken Biryani</h4></td>
                  <td>Mild</td>
                  <th>N</th>
                  <th>€10.95</th>
                </tr>
                <tr>
                  <td style="min-width:165px;"><h4>Lamb Biryani</h4></td>
                  <td>Mild</td>
                  <th>N</th>
                  <th>€11.95</th>
                </tr>
                <tr>
                  <td style="min-width:165px;"><h4>Prawn Biryani</h4></td>
                  <td>Mild</td>
                  <th>N</th>
                  <th>€12.95</th>
                </tr>
                <tr>
                  <td style="min-width:165px;"><h4>Vegetable Biryani</h4></td>
                  <td>Mild</td>
                  <th>V, N</th>
                  <th>€9.50</th>
                </tr>
              </tbody>
            </table>
          </div>
        <hr class="col-md-12">
      </div>

      <!-- Vegegarian Header -->
      <div id="vegetarian-header">
        <img src="http://placehold.it/945x250" alt="" class="img-responsive">
      </div>

      <div class="menu-sections" id="vegetarian">
        <h2>Vegetarian</h2>
        <p>Served with steamed basmati rice or plain naan.</p>
        <div class="table-responsive"> 
            <table class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>Dish</th>
                  <th>Description</th>
                  <th>Spice Level</th>
                  <th>Allergy Code</th>
                  <th>Price</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td style="min-width:165px;"><h4>Shahi Paneer</h4></td>
                  <td>Creamy dish of paneer cheese, cashews, fenugreek, ginger and spices in a creamy tomato sauce.</td>
                  <td>Medium</td>
                  <th>GF, V</th>
                  <th>€9.50</th>
                </tr>
                <tr>
                  <td style="min-width:165px;"><h4>Red Pepper & Cauliflower Makhenwala</h4></td>
                  <td>Creamy tomato based sauce with onion, paprika, ginger and cardamom.</td>
                  <td>Medium</td>
                  <th>GF, V</th>
                  <th>€9.50</th>
                </tr>
                <tr>
                  <td style="min-width:165px;"><h4>Saag Paneer Khumbi</h4></td>
                  <td>Soft Indian cheese, fresh spinach, mushrooms & spices.</td>
                  <td>Medium</td>
                  <th>GF, V</th>
                  <th>€8.95</th>
                </tr>
                <tr>
                  <td style="min-width:165px;"><h4>Vegetable Curry</h4></td>
                  <td>A selection of fresh vegetables & spices in our own special curry sauce.</td>
                  <td>Medium</td>
                  <th>GF, V</th>
                  <th>€8.95</th>
                </tr>
                <tr>
                  <td style="min-width:165px;"><h4>Kadhai Paneer</h4></td>
                  <td>Indian cheese, mix peppers, onion & spices in a dark spicy sauce.</td>
                  <td>Medium</td>
                  <th>GF, V</th>
                  <th>€9.25</th>
                </tr>
                <tr>
                  <td style="min-width:165px;"><h4>Channa Saag Aloo</h4></td>
                  <td>Chickpeas, spinach, potato, tomato, fresh coriander & butter.</td>
                  <td>Mild</td>
                  <th>GF, V</th>
                  <th>€9.25</th>
                </tr>
                <tr>
                  <td style="min-width:165px;"><h4>Sri Lankin Vegetables</h4></td>
                  <td>Vegetables and chickpeas in a coconut sauce with curry leaves, black pepper & fresh coriander.</td>
                  <td>Medium</td>
                  <th>DF, LC, V</th>
                  <th>€8.95</th>
                </tr>
              </tbody>
            </table>
          </div>
        <hr class="col-md-12">
      </div>
      
      <div id="breads" class="menu-sections" style="min-height:500px;">
        <div class="row">
          <div class="col-sm-6">
            <img src="<?php echo get_home_url();?>/wp-content/themes/indianspice/img/naans.jpg" alt="" class="img-responsive img-rounded">
          </div>
          <div class="col-sm-6">
            <h2>Breads</h2>
            <p><em>All breads can now be made Gluten free for 40c extra</em></p>
            <dl class="dl-horizontal">
              <dt>Plain Naan</dt>
              <dd>€2.10</dd>
              <dt>Garlic & Butter Naan</dt>
              <dd>€2.50</dd>
              <dt>Garlic, Onion and Chili Naan</dt>
              <dd>€2.50</dd>
              <dt>Coriander Leaf Naan</dt>
              <dd>€2.50</dd>
              <dt>Fresh Chili Naan</dt>
              <dd>€2.60</dd>
              <dt>Peshwari Naan</dt>
              <dd>€3.60</dd>
              <dt>Tandoori Roti (DF)</dt>
              <dd>€1.75</dd>
              <dt>Lacha Paratha</dt>
              <dd>€2.50</dd>
            </dl>
          </div>
        </div>
      </div>

      <div id="drinks" class="menu-sections">
        <div class="row">
          <!-- image -->
          <div class="col-sm-6">
            <img class="img-responsive" src="http://placehold.it/450x350">
          </div>
          
          <div class="col-sm-6">
            <h2>Drinks</h2>
            <dl class="dl-horizontal">
              <dt>Coca Cola (330ml)</dt>
              <dd>€1.20</dd>
              <dt>7UP (330ml)</dt>
              <dd>€1.20</dd>
              <dt>Fanta (330ml)</dt>
              <dd>€1.20</dd>
            </dl>
          </div>
        
        </div>
      </div>

        <!-- frozen  -->
      <div id="frozen" class="menu-sections" style="min-height:500px;">
        <div class="row">
          <div class="col-sm-6">
            <h2>Frozen Food</h2>
            <dl class="dl-horizontal">
              <dt>Frozen Vegetarian Dish</dt>
              <dd>€5.95</dd>
              <dt>Frozen Chicken Dish</dt>
              <dd>€6.95</dd>
              <dt>Frozen Lamb, Beef or Prawn Dish</dt>
              <dd>€7.95</dd>
            </dl>
          </div>
          <!-- image -->
          <div class="col-sm-6">
            <img class="img-responsive" src="http://placehold.it/450x350">
          </div>
        </div>
      </div>
    

    </div>
  </div>
</section>

