<style>
.blockquote-reverse {
    border-color: #F1E3B6;
}

h2#company {
    font-weight: bold;
    margin-top: 0;
}

h1.hero-header-text {
    font-size: 54px;
    margin-bottom: 0;
}

.phonenumbers > h2 {
    font-size: 24px;
}
</style>
<!-- large hero -->
<section id="welcome" class="hidden-xs">
    <div id="welcome-background" class="jumbotron">
        <div class="container">
            <!-- logo row -->
            <div class="animated bounceIn row">
                <div class="col-sm-12 text-center">
                    <!-- SVG -->
                    <svg height="500" width="500" style="z-index:2; margin-bottom:-325px;">
                        <defs>
                            <pattern id="image" patternUnits="userSpaceOnUse" height="100" width="100">
                                <image x="0" y="0" height="500" width="500"></image>
                            </pattern>
                        </defs>
                        <circle cx="250" cy="250" r="200" fill="#93c54b"></circle>
                    </svg>
                    <!-- hero text -->
                    <h1 class="brand hero-header-text animated bounceIn">indian spice</h1>
                    <h2 id="company">company</h2>
                    <p class="animated bounceIn tagline">GOOD FOOD TO GO</p>
                    <!-- <h2><i class="fa fa-phone animated bounceIn"></i></h2> -->
                    <!-- <div class="animated bounceIn phonenumbers">
                        <p>Greystones
                            <br> 01 201 0868
                            <br> 01 201 0362
                        </p>
                    </div> -->
                </div>
            </div>
            <!-- start button row -->
            <div class="row text-center button-row">
                <div class="col-xs-6">
                    <a href="<?php echo get_home_url();?>/menu">
                        <button class="btn btn-success btn-lg front-page-button animated bounceInLeft">
                            <h4>
                <i class="fa fa-cutlery fa-lg"></i> View Menu
              </h4>
                        </button>
                    </a>
                </div>
                <div class="col-sm-6 text-center col-xs-12">
<a href="http://www.myorder.ie/shop/">                    
<!-- <a href="http://www.just-eat.ie/restaurants-Indianspiceco-Greystones/menu"> -->
                        <button class="btn btn-success btn-lg front-page-button animated bounceInRight">
                            <h4>
                <i class="fa fa-car fa-lg"></i> Order Online
              </h4>
                        </button>
                    </a>
                </div>
            </div>
            <!-- end button row -->
        </div>
        <!-- end welcome container -->
    </div>
    <!-- end welcome jumbotron -->
</section>
<!-- end large hero -->
<style>
section.phones {
    padding-top: 1.75em;
    padding-bottom: 1.75em;
    background-blend-mode: multiply;
    background-color: #222222;
    color: white;
}

.phone-numbers {
    font-size: 1.75rem;
}
</style>
<section class="phones">
    <div class="container">
        <h2 class="text-center"><i class="fa fa-phone"></i> Order Now</h2>
        <div class="row">
            <div class="col-sm-6">
                <h3>Greystones</h3>
                <div class="phone-numbers">
                    <p>(01) 201 0868 & (01) 201 0362</p>
                </div>
            </div>
            <div class="col-sm-6">
                <h3>Newtownmountkennedy</h3>
                <div class="phone-numbers">
                    <p>(01) 201 1511</p>
                </div>
            </div>
        </div>
    </div>
</section>
