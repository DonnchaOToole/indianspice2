<div id="footer" style="z-index:10;">
    <div class="container">
        <div class="text-center">
            <p class="lead home-deliveries">
                <i class="fa fa-truck mr-2"></i>Home deliveries from 5pm
            </p>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="find-us-footer">
                    <a class="find-us-footer-link" href="<?php echo get_home_url();?>/find-us/">
                        <h3 class="white">
                            <i class="white fa fa-map-marker fa-lg mr-2"></i>Find Us
                        </h3>
                    </a>
                    <h4 class="mt-4">Greystones</h4>
                    <a href="tel:0035312010868" class="btn btn-secondary my-2">
                        <i class="fa fa-phone mr-2"></i>(01) 201-0868
                    </a>
                    <address>
                        <strong>Indian Spice Company</strong>
                        <br> Church Road
                        <br> Greystones
                        <br> Wicklow
                        <br>
                    </address>
                    <h4 class="mt-4">Newtownmountkennedy</h4>
                    <a href="tel:0035312011511" class="btn btn-secondary my-2">
                        <i class="fa fa-phone mr-2"></i>(01) 201-1511
                    </a>
                    <address>
                        <strong>Indian Spice Company</strong>
                        <br> 188 Main Street
                        <br> Newtownmountkennedy
                        <br> Wicklow
                        <br>
                    </address>
                </div>
            </div>

            <div class="footer-opening-hours col-md-6">
                <h3 class="white">
                    <i class="fa fa-clock-o fa-lg mr-2 mb-4"></i>Opening Hours
                </h3>
                <h4 class="text-center mb-3">Greystones</h4>
                <table class="table table-sm">
                    <thead>
                        <tr>
                            <th scope="col">Day</th>
                            <th scope="col">Hours</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th>Monday</th>
                            <td>4pm - 10pm</td>
                        </tr>
                        <tr>
                            <th>Tuesday</th>
                            <td>4pm - 10pm</td>
                        </tr>
                        <tr>
                            <th>Wednesday</th>
                            <td>4pm - 10pm</td>
                        </tr>
                        <tr>
                            <th>Thursday</th>
                            <td>4pm - 10pm</td>
                        </tr>
                        <tr>
                            <th>Friday</th>
                            <td>4pm - 10:45pm</td>
                        </tr>
                        <tr>
                            <th>Saturday</th>
                            <td>4pm - 10:45pm</td>
                        </tr>
                        <tr>
                            <th>Sunday</th>
                            <td>4pm - 10pm</td>
                        </tr>
                        <tr>
                            <th>Bank Holiday Monday</th>
                            <td>4pm - 10pm</td>
                        </tr>
                    </tbody>
                </table>

                <h4 class="text-center mb-3 mt-5">Newtownmountkennedy</h4>
                <table class="table table-sm">
                    <thead>
                        <tr>
                            <th scope="col">Day</th>
                            <th scope="col">Hours</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th>Monday</th>
                            <td>Closed</td>
                        </tr>
                        <tr>
                            <th>Tuesday</th>
                            <td>4pm - 10pm</td>
                        </tr>
                        <tr>
                            <th>Wednesday</th>
                            <td>4pm - 10pm</td>
                        </tr>
                        <tr>
                            <th>Thursday</th>
                            <td>4pm - 10pm</td>
                        </tr>
                        <tr>
                            <th>Friday</th>
                            <td>4pm - 10:45pm</td>
                        </tr>
                        <tr>
                            <th>Saturday</th>
                            <td>4pm - 10:45pm</td>
                        </tr>
                        <tr>
                            <th>Sunday</th>
                            <td>4pm - 10pm</td>
                        </tr>
                        <tr>
                            <th>Bank Holiday Monday</th>
                            <td>4pm - 10pm</td>
                        </tr>
                    </tbody>
                </table>

            </div>

        </div>

    </div>

    <div class="social-links text-center pt-5 pb-3">
        <div class="btn-group" role="group" aria-label="Basic example">
            <a href="https://www.instagram.com/indianspiceco/" class="btn btn-secondary">
                <i class="fa fa-instagram"></i>
            </a>
            <a href="https://www.facebook.com/indianspicecompany/" class="btn btn-secondary">
                <i class="fa fa-facebook"></i>
            </a>
            <a href="https://twitter.com/indianspiceltd" class="btn btn-secondary">
                <i class="fa fa-twitter"></i>
            </a>
        </div>
    </div>
</div>

</div>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
    crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
    crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
    crossorigin="anonymous"></script>
</body>

</html>