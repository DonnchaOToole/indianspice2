<?php get_header(); ?>

<section id="catering-hero mt-5">
	<!-- <img src="<?php echo get_home_url();?>/wp-content/themes/indianspice/css/img/stairs.jpg" alt="" class="img-responsive catering-img"> -->
	<div class="jumbotron jumbotron-fluid catering-jumbotron">
		<div class="container">
			<h1>Catering</h1>
			<!-- <p class="lead">Hassle free food</p> -->
		</div>
	</div>
</section>

<section id="catering-info">
	<div class="container">
		<div class="row">
			<div class="col-sm-8">
				<p class="lead">To enquire about our catering service, call <strong>01 201 0868</strong> or <strong>087 912 3696</strong> </p>
				<p>If you're throwing a party, or organising an event and you need some tasty indian food, let us know and we will be glad to cook and deliver crowd pleasing indian dishes straight to your door.</p>
				<p>Please allow us 1 week notice for your event.</p>
				<br>
				<br>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>