<?php
function wpbootstrap_scripts_with_jquery()
{
	// Register the script like this for a theme:
	wp_register_script( 'custom-script', get_template_directory_uri() . '/js/bootstrap.min.js', array( 'jquery' ) );
	// For either a plugin or a theme, you can then enqueue the script:
	wp_enqueue_script( 'custom-script' );
}
add_action( 'wp_enqueue_scripts', 'wpbootstrap_scripts_with_jquery' );

 if ( function_exists('register_sidebar') )
	register_sidebar(array(
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));

function register_my_menus() {
  register_nav_menus(
    array(
      'primary' => __( 'primary' ),
      'footer-menu' => __( 'Footer Menu' )
    )
  );
}
add_action( 'init', 'register_my_menus' );
// Register custom navigation walker
    require_once('wp_bootstrap_navwalker.php');

add_theme_support( 'post-thumbnails' ); 


add_filter('the_content', 'specific_no_wpautop', 9);
function specific_no_wpautop($content) {
    global $post;
    if (is_page('home')) {
        remove_filter( 'the_content', 'wpautop' );
        return $content;
    } else {
        return $content;
    }
}

add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);

function special_nav_class ($classes, $item) {
    if (in_array('current-menu-item', $classes) ){
        $classes[] = 'active ';
    }
    return $classes;
}


?>

