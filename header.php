<!DOCTYPE html>
<html lang="en">
  <head>
  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116692793-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-116692793-1');
</script>

    <meta name="google-site-verification" content="AlMBuWaUTvC8FKDecqxvGK9boMiLt26plFctlJA1Nko" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">    
    <meta name="description" content="Indian Takeaway Greystones - Good Food To Go - We provide quality Indian food from locally sourced ingredients. We can deliver to your door, or you can collect your meal from our friendly staff.  ">
    <meta name="author" content="Focalise.ie"> 
    <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" crossorigin="anonymous">    
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- <meta http-equiv="refresh" content="10" > -->
  
    <title><?php wp_title('|',1,'right'); ?> <?php bloginfo('name'); ?></title>
    <link href="<?php bloginfo('stylesheet_url');?>" rel="stylesheet">
    <?php wp_head(); ?>
  </head>

<nav class="navbar navbar-expand-md navbar-dark bg-dark">
      <a class="navbar-brand" href="<?php echo get_home_url();?>">Indian Spice Company</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="<?php echo get_home_url();?>">Home <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo get_home_url();?>/menu/">Menu</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo get_home_url();?>/about/">About</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo get_home_url();?>/catering/">Catering</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo get_home_url();?>/find-us/">Find Us</a>
          </li>
          <!-- <li class="nav-item">
            <a class="nav-link" href="<?php echo get_home_url();?>/order-now/">Order Now</a>
          </li> -->
        </ul>
      </div>
    </nav>

 <body>
