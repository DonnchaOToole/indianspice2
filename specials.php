
<section id="specialsthisweek">
    <!-- GREYSTONES -->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-center bolder">Our specials this week</h2>
                <h3 class="text-center bolder">Greystones</h3>
            </div>
        </div>
        <div class="row">
            <!-- meat special -->
            <?php
        $meat_special = get_post(17); 
        $m_title = $meat_special->post_title;
        $m_price = $meat_special->post_excerpt;
        $m_desc = $meat_special->post_content;
      ?>
                <div class="col-sm-4">
                    <h3><?php echo $m_title;?></h3>
                    <p class="special-types"><em>Meat Special</em></p>
                    <p>
                        <?php echo $m_desc;?>
                    </p>
                    <p class="special-price"><em><?php echo $m_price;?></em></p>
                </div>
                <!-- veg special  -->
                <?php
        $veg_special = get_post(21); 
        $v_title = $veg_special->post_title;
        $v_price = $veg_special->post_excerpt;
        $v_desc = $veg_special->post_content;
      ?>
                    <div class="col-sm-4">
                        <h3><?php echo $v_title; ?></h3>
                        <p class="special-types"><em>Vegetarian Special</em></p>
                        <p>
                            <?php echo $v_desc; ?>
                        </p>
                        <p class="special-price"><em><?php echo $v_price;?></em></p>
                    </div>
                    <!-- bread special -->
                    <?php
        $bread_special = get_post(67); 
        $m_title = $bread_special->post_title;
        $m_price = $bread_special->post_excerpt;
        $m_desc = $bread_special->post_content;
      ?>
                        <div class="col-sm-4">
                            <h3><?php echo $m_title;?></h3>
                            <p class="special-types"><em>Bread of the Week</em></p>
                            <p>
                                <?php echo $m_desc;?>
                            </p>
                            <p class="special-price"><em><?php echo $m_price;?></em></p>
                        </div>
        </div>
    </div>
    <!-- NEWTOWN -->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="text-center bolder">Newtownmountkennedy</h3>
            </div>
        </div>
        <div class="row">
            <!-- meat special -->
            <?php
        $meat_special = get_post(319); 
        $m_title = $meat_special->post_title;
        $m_price = $meat_special->post_excerpt;
        $m_desc = $meat_special->post_content;
      ?>
                <div class="col-sm-4">
                    <h3><?php echo $m_title;?></h3>
                    <p class="special-types"><em>Meat Special</em></p>
                    <p>
                        <?php echo $m_desc;?>
                    </p>
                    <p class="special-price"><em><?php echo $m_price;?></em></p>
                </div>
                <!-- veg special  -->
                <?php
        $veg_special = get_post(322); 
        $v_title = $veg_special->post_title;
        $v_price = $veg_special->post_excerpt;
        $v_desc = $veg_special->post_content;
      ?>
                    <div class="col-sm-4">
                        <h3><?php echo $v_title; ?></h3>
                        <p class="special-types"><em>Vegetarian Special</em></p>
                        <p>
                            <?php echo $v_desc; ?>
                        </p>
                        <p class="special-price"><em><?php echo $v_price;?></em></p>
                    </div>
                    <!-- bread special -->
                    <?php
        $bread_special = get_post(324); 
        $m_title = $bread_special->post_title;
        $m_price = $bread_special->post_excerpt;
        $m_desc = $bread_special->post_content;
      ?>
                        <div class="col-sm-4">
                            <h3><?php echo $m_title;?></h3>
                            <p class="special-types"><em>Bread of the Week</em></p>
                            <p>
                                <?php echo $m_desc;?>
                            </p>
                            <p class="special-price"><em><?php echo $m_price;?></em></p>
                        </div>
        </div>
    </div>
</section>
