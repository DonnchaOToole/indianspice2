<?php get_header(); ?>

<section id="welcome">
    <div id="welcome-background" class="jumbotron jumbotron-fluid">
        <div class="container">
            <div class="brand hero-header-text text-center">
                <h1>indian spice</h1>
                <h2>company</h2>
                <p class="tagline">GOOD FOOD TO GO</p>
            </div>
        </div>
    </div>
</section>
<section class="phones">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="greystones-phone-number branch-phone-number">
                    <h5 class="text-uppercase">
                        Greystones</h5>
                    <div class="phone-numbers">
                        <p>(01) 201 0868 &amp; (01) 201 0362</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="newtown-phone-number branch-phone-number">
                    <h5 class="text-uppercase">
                        Newtownmountkennedy</h5>
                    <div class="phone-numbers">
                        <p>(01) 201 1511</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="specials">
    <div class="container">
        <h2 class="mb-4 display-3">Specials</h2>
        <div class="branch-specials greystones-specials">
            <h5 class="text-uppercase branch-location">
                <i class="fa fa-map-marker mr-2"></i>Greystones Branch</h5>
            <h4>
                <small class="text-muted special-category">Meat Special</small>
            </h4>
            <h4>Murghmalu tikka</h4>
            <p>Chicken cubes marinated with yoghurt, mild aromatic spices, cheddar cheese, and cooked in the clay oven. Served with plentiful salad and flakey butter naan. </p>
            <p>€10.95</p>
            <h4>
                <small class="text-muted special-category">Vegetarian Special</small>
            </h4>
            <h4>Aloo Mutterka Jhol</h4>
            <p>Green peas and semi mashed potatoes, tempered with coriander seeds, garlic flakes and cooked with fresh tomatoes, onion and cumin powder. </p>
            <p>€9.50</p>
            <h4>
                <small class="text-muted special-category">Vegetable Kulcha Naan</small>
            </h4>
            <h4>Naan stuffed with vegetable.</h4>
            <p>€3.50</p>
            <!-- <p>Naan bread stuffed with spices and fresh corianter and topped with butter.</p> -->
        </div>
        <div class="branch-specials newtownmountkennedy-specials">
            <h5 class="branch-location">
                <i class="fa fa-map-marker mr-2"></i>Newtownmountkennedy Branch</h5>
            <h4>
                <small class="text-muted special-category">Meat Special</small>
            </h4>
            <h4>Chiken Kofta Curry</h4>
            <p>Minced chicken kofta marinated with ginger, garlic and spices, cooked in an onion tomato sauce with yogurt and flavoured with star ansise. 
            </p>
            <p>€10.95</p>
            <h4>
                <small class="text-muted special-category">Vegetarian Special</small>
            </h4>
            <h4>Channa Chole</h4>
            <p>Chickpeas and potatoes tempered with coriander seed cooked in an onion tomato sauce flavored with roast cumin and coriander.</p>
            <p>€8.95</p>
            <h4>
                <small class="text-muted special-category">Naan Special</small>
            </h4>
            <h4>Poppy Seed Naan</h4>
            <p>€2.75</p>
        </div>
    </div>
</section>
<!-- <section class="main-buttons">
    <div class="container">
        <ul>
            <li>
                <a class="btn btn btn-outline-primary text-uppercase" href="<?php echo get_home_url();?>/menu/">
                    View Menu
                </a>
            </li>
            <li>
                <a class="btn btn btn-outline-primary text-uppercase" href="http://www.myorder.ie/shop/">
                    Order Online
                </a>
            </li>
        </ul>
    </div>
</section> -->


<?php // require_once('large-front-page-hero.php'); ?>
<?php // require_once('small-front-page-hero.php'); ?>
<?php // require_once('phonenumberformobile.php'); ?>
<?php // require_once('specials.php'); ?>
<!-- <section class="hidden-xs" id="bestinireland">
  <div class="container">
    <div class="col-sm-offset-5 col-sm-2">
      <img style="margin-top:20px; margin-bottom:20px;" class='img-responsive' src="<?php echo get_home_url();?>/wp-content/themes/indianspice/img/bestinireland.jpg" alt="">
    </div>
  </div>
</section>
 -->
<section id="about-text">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <p class="text-center lead pb-4 mb-4">Indian Spice Company has been offering the finest classical Indian cuisine since 2002.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <h1 class='text-center'>
                    <i class="icons fa fa-lg fa-certificate"></i>
                </h1>

                <p>We aim to provide quality Indian food from locally sourced ingredients.</p>
                <p>We can deliver to your door, or you
                    can collect your meal from our friendly staff. </p>
            </div>
            <div class="col-sm-4">
                <h1 class='text-center'>
                    <i class="icons fa fa-lg fa-fire"></i>
                </h1>
                <p>Our chefs Uttam Bisht and Rajendra Singh have a passion for the zing and zest of Indian street food, and
                    our dishes are crafted with the sharpness and precision of the best ethnic cooking.</p>
            </div>
            <div class="col-sm-4">
                <h1 class='text-center'>
                    <i class="icons fa fa-lg fa-clock-o"></i>
                </h1>
                <p>Good food takes time. Since our food is cooked fresh to order, please allow us enough time to infuse the
                    spices to best suit your palate.</p>
                    <p>The standard collection time is 35 mins, and delivery times are 45 mins
                    up to an hour at busy times.</p>
            </div>
        </div>
    </div>
</section>
<section id="quote">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <blockquote class="blockquote">
                    <p>"Ace Indian Takeaway"</p>
                    <footer><a href="https://www.tomdoorley.com/about/">Tom Doorley</a></footer>
                </blockquote>
            </div>
        </div>
    </div>
</section>


<?php get_footer(); ?>