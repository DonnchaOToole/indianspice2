<?php get_header(); ?>  


<style>
  div.gmap {
    width: 100%;
  }
  iframe.mapiframe {
  width: 100%;
  min-height: 500px;
  }
  #map {
    margin-top: 55px;
  }

body {
  background-color: #222;
}

body > div.row > div > p.lead {

  margin-top: 10px;
  color: white;
}

section#map {
  margin-bottom: 15px;
}
</style>



<section id="map">
<div class="container">
<h3 class="map-title mt-5 pt-4">Greystones Branch</h3>
<div class="phone-number text-white pb-3">
<i class="fa fa-phone mr-2"></i> (01) 201-0868
</div>
</div>
<div class="embed-responsive embed-responsive-21by9">
<iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2393.0486219994223!2d-6.063675284385666!3d53.14522192811635!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0000000000000000%3A0x85c3ffca1b49e1e9!2sIndian+Spice!5e0!3m2!1sen!2sie!4v1417010802153" frameborder="0" style="border:0"></iframe>
</div>

<div class="container pt-5 pb-3">
<h3 class="map-title">Newtownmountkennedy Branch</h3>
<div class="phone-number text-white pb-3">
<i class="fa fa-phone mr-2"></i> (01) 201-1511
</div>
</div>
<div class="embed-responsive embed-responsive-21by9">
<iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1198.0239159256457!2d-6.113380910455807!3d53.09137592017044!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xe08405b5a8f48ff4!2sIndian+Spice+Company!5e0!3m2!1sen!2sie!4v1517247208152" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>
</section>    


<?php get_footer(); ?>
