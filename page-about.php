<?php get_header(); ?>

<!-- 
<section class="about-header">
	<img src="<?php echo get_home_url();?>/wp-content/themes/indianspice/img/team.jpg" alt="" class="img-responsive">
</section> -->


<section id="about-text">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 pt-5 pb-5">
				<h1 class="text-center">Indian Spice Company</h1>
				<p class="text-center">Greystones & Newtownmountkennedy</p>
				<p class="text-center lead">Indian Spice Company has been offering the finest classical Indian cuisine since 2002.</p>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4">
				<h1 class='text-center'><i class="icons fa fa-lg fa-certificate"></i></h1>

				<p>We aim to provide quality Indian food from locally sourced ingredients. We can deliver to your door, or you can collect your meal from our friendly staff.  </p>
			</div>
			<div class="col-sm-4">
				<h1 class='text-center'><i class="icons fa fa-lg fa-fire"></i></h1>
				<p>Our chefs Uttam Bisht and Rajendra Singh have a passion for the zing and zest of Indian street food, and our dishes are crafted with the sharpness and precision of the best ethnic cooking.</p>
			</div>
			<div class="col-sm-4">
				<h1 class='text-center'><i class="icons fa fa-lg fa-clock-o"></i></h1>
				<p class="lead">Good food takes time. </p>
				<p>Since our food is cooked fresh to order, please allow us enough time to infuse the spices to best suit your palate.</p>
				<p>The standard collection time is 35 mins, and delivery times are 45 mins up to an hour at busy times.</p>
			</div>
		</div>
	</div>
</section>


<?php get_footer(); ?>
