<?php get_header(); ?>
<!-- <section id="menu-hero" class="primary-color-background hidden-xs">
    <div id="menu-hero-background" class="jumbotron primary-color-background pt-4 pb-4">
    </div>
</section> -->
<!-- <div class="container-fluid">
    Greystones: 01 201 0868 Newtownmountkennedy: 01 201 1511
</div> -->

<section id="menu" class="pb-5 mt-4">
    <div class="container">
        <div id="starters">
            <h2 class="menu-section-header-text">Starters</h2>
            <table class="table table-hover table-sm table-responsive">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Allergy Code</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <h5>Indian Spice Wings</h5>
                        </td>
                        <td>Chicken wings marinated in ginger, paprika &amp; garlic, deep fried and coated in a honey, soy sauce,
                            sesame seed and chili sauce. Served with a mint dip. <br><em> Spiciness: Medium</em></td>
                        <th>DF, N</th>
                        <th>€5.95</th>
                    </tr>
                    <tr>
                        <td>
                            <h5>Real Samosas</h5>
                        </td>
                        <td>Spiced vegetables &amp; peanuts in homemade pastry, deep fried. Served with tamarind dip. <br><em> Spiciness: Medium</em>
                        </td>
                        <th>V, DF, N</th>
                        <th>
                            1: €2.75
                            <br/> 2: €4.50
                        </th>
                    </tr>
                    <tr>
                        <td>
                            <h5>Onion Bhaji</h5>
                        </td>
                        <td>Our home made onion fritters with fresh mint &amp; coriander dip.  <br><em> Spiciness: Mild</em></td>
                        <th>V, DF, N</th>
                        <th>€3.95</th>
                    </tr>
                    <tr>
                        <td>
                            <h5>Pakoras</h5>
                        </td>
                        <td>Indian street food. Sliced vegetables fried in gram flour batter. <br><em> Spiciness: Mild</em></td>
                        <th>V, DF, N</th>
                        <th>€3.95</th>
                    </tr>
                    <tr>
                        <td>
                            <h5>Pepper Exchange Prawns</h5>
                        </td>
                        <td>Prawns stir fried with tomato, coconut, black pepper, lime juice and coriander.  <br><em> Spiciness: Mild</em></td>
                        <th>GF</th>
                        <th>€6.95</th>
                    </tr>
                    <tr>
                        <td>
                            <h5>Poppadoms</h5>
                        </td>
                        <td>Sundried poppadoms cooked on premises &amp; served with mint dip.</td>
                        <th>V, DF</th>
                        <th>€1.95</th>
                    </tr>
                    <tr>
                        <td>
                            <h5>Hariyali Murgh Tikka</h5>
                        </td>
                        <td>Chicken marinated with yoghurt, mixed herbs, cumin and ginger garlic. Baked in our clay oven. Served
                            with a mint dip. <br><em> Spiciness: Mild</em></td>
                        <th>€5.50</th>
                    </tr>
                    <tr>
                        <td>
                            <h5>Chickpea, Cucumber &amp; Pepper Salad</h5>
                        </td>
                        <td>Tempered with mustard seeds, coriander &amp; lime juice. <br><em> Spiciness: Medium</em></td>
                        <th>-</th>
                        <th>€4.95</th>
                    </tr>
                    <tr>
                        <td>
                            <h5>Sambusas</h5>
                        </td>
                        <td>Persian street food pastry. Miced local irish lamb, scallions, parsley, turmeric, black pepper in
                            pastry and deep fried. Served with mint and cumin dip.  <br><em> Spiciness: Mild</em></td>
                        <th>N</th>
                        <th>1 - €2.95
                            <br> 2 - €5.80</th>
                    </tr>
                    <tr>
                        <td>
                            <h5>Starter Combo A</h5>
                        </td>
                        <td>2 samosas, 2 bhajias &amp; dips.</td>
                        <th>N, V, DF</th>
                        <th>€7.60</th>
                    </tr>
                    <tr>
                        <td>
                            <h5>Starter Combo B</h5>
                        </td>
                        <td>2 samosas, 2 bhajias, 2 sambusas &amp; dips.</td>
                        <th>N, DF</th>
                        <th>€12.80</th>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="mt-5" id="chicken">
            <h2 class="menu-section-header-text">Chicken
                <small>(Murgh)</small>
            </h2>
            <div class="table-responsive">
                <table class="table table-hover table-sm">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Allergy Code</th>
                            <th>Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <h5>Murgh Makhani (Butter Chicken)</h5>
                            </td>
                            <td>Marinated chicken breast in yoghurt, paprika, cashew nuts, cardamom, cinnamon, ginger &amp; spices,
                                with a little butter.<br><em>Spiciness: Mild</em></td>
                            <th>GF, N</th>
                            <th>€10.95</th>
                        </tr>
                        <tr>
                            <td>
                                <h5>Sri Lankan Chicken</h5>
                            </td>
                            <td>Chicken breast pieces, coconut milk, curry leaves, crushed black pepper &amp; fresh coriander.<br><em>Spiciness: Medium</em></td>
                            <th>GF, DF</th>
                            <th>€10.95</th>
                        </tr>
                        <tr>
                            <td>
                                <h5>Mumbai Chicken Korma</h5>
                            </td>
                            <td>Rich and fragrant chicken dish with coconut, ground almonds, sultanas &amp; cream.<br><em>Spiciness: Mild</em></td>
                            <th>GF, N</th>
                            <th>€10.95</th>
                        </tr>
                        <tr>
                            <td>
                                <h5>Murgh Kadhai</h5>
                            </td>
                            <td>Dark and spicy dish of chicken breast &amp; sweet peppers.<br><em>Spiciness: Medium</em></td>
                            <th>GF, LC</th>
                            <th>€10.95</th>
                        </tr>
                        <tr>
                            <td>
                                <h5>Murgh Rassa</h5>
                            </td>
                            <td>Chicken and mixed vegetables tempered with mustard seeds and curry leaves finished with fresh
                                tomatoes and coconut milk.<br><em>Spiciness: Mild</em></td>
                            <th></th>
                            <th>€10.95</th>
                        </tr>
                        <tr>
                            <td>
                                <h5>Chicken Vindaloo</h5>
                            </td>
                            <td>Our hottest chicken dish with wine vinegar, potato, coconut, garlic, ginger &amp; chillies.<br><em>Spiciness: Hot</em></td>
                            <th>GF</th>
                            <th>€10.95</th>
                        </tr>
                        <tr>
                            <td>
                                <h5>Chicken Madras</h5>
                            </td>
                            <td>Spicy dish with coconut, black pepper, curry leaves, mustard seeds and chillies.<br><em>Spiciness: Medium</em></td>
                            <th>GF</th>
                            <th>€10.95</th>
                        </tr>
                        <tr>
                            <td>
                                <h5>Chicken Tikka Masala</h5>
                            </td>
                            <td>Chicken in creamy tomato, fenugreek, cashew nut and cardamom sauce.<br><em>Spiciness: Mild</em></td>
                            <th>N, GF</th>
                            <th>€10.95</th>
                        </tr>
                        <tr>
                            <td>
                                <h5>Curry Chicken by Mrs Singh</h5>
                            </td>
                            <td>Our chef's mum's recipe. This is how it should taste.<br><em>Spiciness: Mild</em></td>
                            <th>GF, LC</th>
                            <th>€10.95</th>
                        </tr>
                        <tr>
                            <td>
                                <h5>Chicken Jalfrezi</h5>
                            </td>
                            <td>Chicken breast marinated in tangy &amp; sweet tomato sauce with mixed peppers &amp; red onion.<br><em>Spiciness: Medium</em>
                            </td>
                            <th>GF</th>
                            <th>€10.95</th>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="mt-5" id="meat">
            <h2 class="menu-section-header-text">Meat
                <small>(Gosht)</small>
            </h2>
            <div class="table-responsive">
                <table class="table table-hover table-sm">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Allergy Code</th>
                            <th>Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <h5>Lamb Kofta Chilli Masala</h5>
                            </td>
                            <td>Minced lamb meatbals in a yoghurt based sauce with fresh coriander, mint &amp; green chillies.<br><em>Spiciness: Hot</em>
                            </td>
                            <th>GF</th>
                            <th>€10.95</th>
                        </tr>
                        <tr>
                            <td>
                                <h5>Gosht Saagwalla</h5>
                            </td>
                            <td>Diced local lamb with spinach, sundried fenugreek leaves, garlic &amp; ginger.<br><em>Spiciness: Medium</em></td>
                            <th>GF</th>
                            <th>€11.95</th>
                        </tr>
                        <tr>
                            <td>
                                <h5>Beef Madras</h5>
                            </td>
                            <td>Spicy dish of tender Irish beef with coconut, black pepper, curry leaves, mustard seeds &amp;
                                chillies.<br><em>Spiciness: Medium</em>
                            </td>
                            <th>GF</th>
                            <th>€10.95</th>
                        </tr>
                        <tr>
                            <td>
                                <h5>Lamb Madras</h5>
                            </td>
                            <td>Spicy dish with coconut, black pepper, curry leaves, mustard seeds &amp; chillies.<br><em>Spiciness: Medium</em></td>
                            <th>GF</th>
                            <th>€11.95</th>
                        </tr>
                        <tr>
                            <td>
                                <h5>Lamb Korma</h5>
                            </td>
                            <td>Rich and fragrant lamb dish with coconut, ground almonds, sultanas &amp; cream.<br><em>Spiciness: Mild</em> </td>
                            <th>GF, N</th>
                            <th>€11.95</th>
                        </tr>
                        <tr>
                            <td>
                                <h5>Railway Lamb</h5>
                            </td>
                            <td>Our Rogan Josh signature dish - Diced local lamb slowly simmered with tomatoes, red onions, garam
                                masala &amp; spices, finished with local coriander.<br><em>Spiciness: Medium</em></td>
                            <th>GF, LC</th>
                            <th>€11.95</th>
                        </tr>
                        <tr>
                            <td>
                                <h5>Beef Vindaloo</h5>
                            </td>
                            <td>Fiery and tangy Goan dish. Tender strips of beef with potato, wine vinegar, coconut, garlic,
                                ginger &amp; chillies.<br><em>Spiciness: Hot</em></td>
                            <th>GF</th>
                            <th>€11.95</th>
                        </tr>
                        <tr>
                            <td>
                                <h5>Balti Beef</h5>
                            </td>
                            <td>Tender Irish beef in a red pepper, tomato, red onion based sauce with fenugreek, fresh coriander
                                and garlic.<br><em>Spiciness: Medium</em></td>
                            <th>GF</th>
                            <th>€11.95</th>
                        </tr>
                        <tr>
                            <td>
                                <h5>Gosht Achari</h5>
                            </td>
                            <td>Tender lamb cooked in a chunky tomato sauce flavoured with pickling spices and yoghurt.<br><em>Spiciness: Medium</em></td>
                            <th>GF</th>
                            <th>€11.95</th>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="mt-5" id="prawn">
            <h2 class="menu-section-header-text">Prawn
                <small>(Jinga)</small>
            </h2>
            <p class="lead">Served with steamed basmati rice or plain naan.</p>
            <div class="table-responsive">
                <table class="table table-hover table-sm">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Allergy Code</th>
                            <th>Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <h5>Prawn Jalfrezi</h5>
                            </td>
                            <td>Tiger prawns in a tangy sweet tomato sauce with mixed peppers &amp; sweet red onions.<br><em>Spiciness: Medium</em></td>
                            <th>GF</th>
                            <th>€12.95</th>
                        </tr>
                        <tr>
                            <td>
                                <h5>Goan Prawn Curry</h5>
                            </td>
                            <td>Popular dish with tiger prawns, coconut, tamarind, chillies &amp; curry leaves.<br><em>Spiciness: Medium</em></td>
                            <th>GF, LC</th>
                            <th>€12.95</th>
                        </tr>
                        <tr>
                            <td>
                                <h5>Jinga Makhani</h5>
                            </td>
                            <td>Tiger prawns in a rich, creamy tomato sauce with smoked paprika, crushed fenugreek leaves and
                                toasted cashew nuts.<br><em>Spiciness: Medium</em></td>
                            <th>GF, N</th>
                            <th>€12.95</th>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="mt-5" id="biryani">
            <h2 class="menu-section-header-text">Biryani</h2>
            <p class="lead">The complete dish.</p>
            <p>Made with basmati rice, peppers, mushrooms, tomatoes, biryani spices and cashew nuts.</p>
            <p>You select the main ingredient from chicken, lamb, prawn or vegetable. Served with a mint raita dip and poppadom.</p>
            <div class="table-responsive">
                <table class="table table-hover table-sm">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Spice Level</th>
                            <th>Allergy Code</th>
                            <th>Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <h5>Chicken Biryani</h5>
                            </td>
                            <td>Mild</td>
                            <th>N</th>
                            <th>€10.95</th>
                        </tr>
                        <tr>
                            <td>
                                <h5>Lamb Biryani</h5>
                            </td>
                            <td>Mild</td>
                            <th>N</th>
                            <th>€11.95</th>
                        </tr>
                        <tr>
                            <td>
                                <h5>Prawn Biryani</h5>
                            </td>
                            <td>Mild</td>
                            <th>N</th>
                            <th>€12.95</th>
                        </tr>
                        <tr>
                            <td>
                                <h5>Vegetable Biryani</h5>
                            </td>
                            <td>Mild</td>
                            <th>V, N</th>
                            <th>€9.50</th>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="mt-5" id="vegetarian">
            <h2 class="menu-section-header-text">Vegetarian</h2>
            <p class="lead">Served with steamed basmati rice or plain naan.</p>
            <div class="table-responsive">
                <table class="table table-hover table-sm">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Allergy Code</th>
                            <th>Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <h5>Shahi Paneer</h5>
                            </td>
                            <td>Creamy dish of paneer cheese, cashews, fenugreek, ginger &amp; spices in a creamy tomato sauce.<br><em>Spiciness: Medium</em>
                            </td>
                            <th>GF, V</th>
                            <th>€9.50</th>
                        </tr>
                        <tr>
                            <td>
                                <h5>Channa Shakarkand</h5>
                            </td>
                            <td>Chickpeas and sweet potatoes tempered with fresh garlic and mustard seeds cooked with onions,
                                tomatoes, finished with aromatic spices and coriander.<br><em>Spiciness: Mild</em></td>
                            <th></th>
                            <th>€8.95</th>
                        </tr>
                        <tr>
                            <td>
                                <h5>Red Pepper &amp; Cauliflower Makhenwala</h5>
                            </td>
                            <td>Creamy tomato based sauce with onion, paprika, ginger and cardamom.<br><em>Spiciness: Medium</em></td>
                            <th>GF, V</th>
                            <th>€9.50</th>
                        </tr>
                        <tr>
                            <td>
                                <h5>Saag Paneer Khumbi</h5>
                            </td>
                            <td>Soft Indian cheese, fresh spinach, mushrooms &amp; spices.<br><em>Spiciness: Medium</em></td>
                            <th>GF, V</th>
                            <th>€8.95</th>
                        </tr>
                        <tr>
                            <td>
                                <h5>Vegetable Curry</h5>
                            </td>
                            <td>A selection of fresh vegetables &amp; spices in our own special curry sauce.<br><em>Spiciness: Medium</em></td>
                            <th>GF, V</th>
                            <th>€8.95</th>
                        </tr>
                        <tr>
                            <td>
                                <h5>Kadhai Paneer</h5>
                            </td>
                            <td>Indian cheese, mix peppers, onion &amp; spices in a dark spicy sauce.<br><em>Spiciness: Medium</em></td>
                            <th>GF, V</th>
                            <th>€9.25</th>
                        </tr>
                        <tr>
                            <td>
                                <h5>Channa Saag Aloo</h5>
                            </td>
                            <td>Chickpeas, spinach, potato, tomato, fresh coriander &amp; butter.<br><em>Spiciness: Mild</em></td>
                            <th>GF, V</th>
                            <th>€9.25</th>
                        </tr>
                        <tr>
                            <td>
                                <h5>Sri Lankan Vegetables</h5>
                            </td>
                            <td>Vegetables and chickpeas in a coconut sauce with curry leaves, black pepper &amp; fresh coriander.<br><em>Spiciness: Medium</em>
                            </td>
                            <th>DF, LC, V</th>
                            <th>€8.95</th>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="mt-5" id="sides">
            <h2 class="menu-section-header-text">Sides</h2>
            <p class="lead">Main courses include a naan or rice.</p>
            <div class="table-responsive">
                <table class="table table-hover table-sm">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Allergy Code</th>
                            <th>Side</th>
                            <th>Main</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <h5>Bombay Aloo</h5>
                            </td>
                            <td>Spicy potato dish with onions and chilli.<br><em>Spiciness: Hot</em></td>
                            <th>GF, V, N</th>
                            <th>€4.50</th>
                            <th>€8.95</th>
                        </tr>
                        <tr>
                            <td>
                                <h5>Bhindi Do Pyaza</h5>
                            </td>
                            <td>Okra stir fried with onions &amp; cumin seeds.<br><em>Spiciness: Medium</em></td>
                            <th>V</th>
                            <th>€4.50</th>
                            <th>€8.95</th>
                        </tr>
                        <tr>
                            <td>
                                <h5>Saag Aloo</h5>
                            </td>
                            <td>Potatoes sautéed with spinach and herbs.<br><em>Spiciness: Mild</em></td>
                            <th>GF, V</th>
                            <th>€4.50</th>
                            <th>€8.95</th>
                        </tr>
                        <tr>
                            <td>
                                <h5>Brinjal Masala</h5>
                            </td>
                            <td>Aubergine dish with tomato and spices.<br><em>Spiciness: Mild</em></td>
                            <th>GF, V, DF, N</th>
                            <th>€4.50</th>
                            <th>€8.95</th>
                        </tr>
                        <tr>
                            <td>
                                <h5>Tarka Dal</h5>
                            </td>
                            <td>Lentils tempered with cumin seeds.<br><em>Spiciness: Mild</em></td>
                            <th>GF, V, DF, N</th>
                            <th>€4.50</th>
                            <th>€8.95</th>
                        </tr>
                        <tr>
                            <td>
                                <h5>Channa Masala</h5>
                            </td>
                            <td>Chickpeas, onion, tomato sauce, fresh coriander & garlic.<br><em>Spiciness: Mild</em></td>
                            <th></th>
                            <th>€4.50</th>
                            <th>€8.95</th>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="mt-5" id="clampdown">
            <h2 class="menu-section-header-text">Clampdown Specials</h2>
            <p class="lead">We also offer most of our main courses as a ‘Clampdown Special’ which is a smaller portion dish with rice AND
                naan.
            </p>
            <table class="table table-hover table-sm">
                <tbody>
                    <tr>
                        <td>Chicken Dish Clampdown</td>
                        <td>€8.95</td>
                    </tr>
                    <tr>
                        <td>Beef Dish Clampdown</td>
                        <td>€9.95</td>
                    </tr>
                    <tr>
                        <td>Lamb Dish Clampdown</td>
                        <td>€9.95</td>
                    </tr>
                    <tr>
                        <td>Prawn Dish Clampdown</td>
                        <td>€10.95</td>
                    </tr>
                </tbody>
            </table>

        </div>

        <h2 class="menu-section-header-text mb-4 mt-4">Breads</h2>
        <table class="table table-hover table-sm">
            <tbody>
                <tr>
                    <td>Plain Naan</td>
                    <td>€2.10</td>
                </tr>
                <tr>
                    <td>Garlic &amp; Butter Naan</td>
                    <td>€2.50</td>
                </tr>
                <tr>
                    <td>Garlic, Onion &amp; Chili Naan</td>
                    <td>€2.50</td>
                </tr>
                <tr>
                    <td>Coriander Leaf Naan</td>
                    <td>€2.50</td>
                </tr>
                <tr>
                    <td>Fresh Chili Naan</td>
                    <td>€2.60</td>
                </tr>
                <tr>
                    <td>Peshwari Naan</td>
                    <td>€3.60</td>
                </tr>
                <tr>
                    <td>Tandoori Roti (DF)</td>
                    <td>€1.75</td>
                </tr>
                <tr>
                    <td>Fresh Chili Naan</td>
                    <td>€2.60</td>
                </tr>
                <tr>
                    <td>Lacha Paratha</td>
                    <td>€2.50</td>
                </tr>
                <tr>
                    <td>Keema Naan</td>
                    <td>€3.60</td>
                </tr>
            </tbody>
        </table>
        <h2 class="menu-section-header-text mb-4 mt-4">Rice</h2>
        <table class="table table-hover table-sm">
            <tbody>
                <tr>
                    <td>Steamed Basmati</td>
                    <td>€2.10</td>
                </tr>
                <tr>
                    <td>Pulao Rice</td>
                    <td>€2.30</td>
                </tr>
                <tr>
                    <td>Organic Brown Rice</td>
                    <td>€2.30</td>
                </tr>
                <tr>
                    <td>Lemon Rice</td>
                    <td>€2.50</td>
                </tr>
                <tr>
                    <td>Poppadoms &amp; Dip</td>
                    <td>€1.95</td>
                </tr>
            </tbody>
        </table>

        <h2 class="menu-section-header-text mb-4 mt-4">Drinks</h2>
        <table class="table table-hover table-sm">
            <tbody>
                <tr>
                    <td>Coca Cola (330ml)</td>
                    <td>€1.20</td>
                </tr>
                <tr>
                    <td>7UP (330ml)</td>
                    <td>€1.20</td>
                </tr>
                <tr>
                    <td>Fanta (330ml)</td>
                    <td>€1.20</td>
                </tr>
            </tbody>
        </table>

</section>

<?php get_footer(); ?>